﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Q02_Receipes
{
    [Activity(Label = "Display Recipe Details")]
    public class recipeDetails : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            string incomingDish = Intent.GetStringExtra("SEND_DISH") ?? "No Dish Recieved";
            long incomingDishID = Intent.GetLongExtra("DISH_ID", 0);
            SetContentView(Resource.Layout.recipeDetails);
            TextView dishName = (TextView)FindViewById<TextView>(Resource.Id.dishTitle);
            TextView recipeDetail = (TextView)FindViewById<TextView>(Resource.Id.recipeDetails);
            dishName.Text = incomingDish;
            Button voteSMS = FindViewById<Button>(Resource.Id.btnVoteSMS);
            Button voteCall = FindViewById<Button>(Resource.Id.btnVoteCall);
            Button voteEmail = FindViewById<Button>(Resource.Id.btnVoteEmail);

            var dishPic = FindViewById<ImageView>(Resource.Id.imgDish);
            switch (incomingDishID)
            {
                case 1:
                    dishPic.SetImageResource(Resource.Drawable.beandip);
                    recipeDetail.Text = "1) In medium bowl, mix 1 can (16 oz) Old El Paso™ refried beans and 1 package (1 oz) Old El Paso™ taco seasoning mix. Spread mixture on large platter."
                         + "2) In another medium bowl, mix 1 package(8 oz) cream cheese, softened and 1 can(4.5 oz) Old El Paso™ chopped green chiles.Carefully spread over bean mixture."
                         + "3) Top with 1 cup Old El Paso™ Thick 'n Chunky salsa (any variety), 2 cups shredded lettuce, 2 cups shredded Cheddar or Mexican cheese blend (8 oz), 1 can (2.25 oz) sliced ripe olives, drained (1/2 cup) and 1 medium tomato, diced (3/4 cup)."
 + "4) Refrigerate until serving time. Serve with tortilla chips.";
                    break;

                case 2:
                    dishPic.SetImageResource(Resource.Drawable.pulledpork);
                    recipeDetail.Text = "You can find this great recipe at https://www.bettycrocker.com/recipes/slow-cooker-pulled-pork-sandwiches/2721afab-a5e0-4c0e-92f8-b08c67882508";
                    break;

                default:
                    dishPic.SetImageResource(Resource.Drawable.applepie);
                    recipeDetail.Text = "You can find this great recipe at https://www.bettycrocker.com/recipes/scrumptious-apple-pie/c9a4acc6-85aa-4128-b0b0-1a17bdbe05e0";
                    break;
            }



            voteCall.Click += delegate
            {
                var uri = Android.Net.Uri.Parse("tel:3055551212");
                Intent dialVote = new Intent(Intent.ActionDial, uri);
                StartActivity(dialVote);
            };
            voteSMS.Click += delegate
            {
                var uri = Android.Net.Uri.Parse("sms:3055551212");
                Intent smsVote = new Intent(Intent.ActionView, uri);
                StartActivity(smsVote);

            };
            voteEmail.Click += delegate
            {
                var email = new Intent(Android.Content.Intent.ActionSend);
                email.PutExtra(Android.Content.Intent.ExtraEmail, new string[] { "paul.christian001@mymdc.net" });
                email.PutExtra(Android.Content.Intent.ExtraSubject, string.Format("I'm voting for {0}", dishName));
                email.SetType("message/rfc822");
                StartActivity(email);
            };






        }

    }
}