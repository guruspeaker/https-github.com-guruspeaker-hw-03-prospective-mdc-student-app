﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace sc00_Planets.Droid
{
	[Activity (Label = "Paul's Planet Program", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		int count = 1;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner);
            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(PlanetSelected);
            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.planets_array, Android.Resource.Layout.SimpleSpinnerItem);
            spinner.Adapter = adapter;

            // Get our button from the layout resource,
            // and attach an event to it

        }
        private void PlanetSelected (object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner_local = (Spinner)sender;
            string toast = string.Format("The Planet you selected is {0}", spinner_local.GetItemAtPosition(e.Position));
            Toast.MakeText(this, toast, ToastLength.Long).Show();
            
        }
	}
}


