﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Webkit;

namespace MyWebAppAndroid
{
    [Activity(Label = "MyWebAppAndroid", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            WebView webView = FindViewById<WebView>(Resource.Id.myWebView);

            webView.SetWebViewClient(new WebViewClient());
            webView.LoadUrl("http://www.mdc.edu");

            // enable java script
            webView.Settings.JavaScriptEnabled = true;

            // allow zoom/pan out
            webView.Settings.BuiltInZoomControls = true;
            webView.Settings.SetSupportZoom(true);

            // all scrolling
            webView.ScrollBarStyle = Android.Views.ScrollbarStyles.InsideOverlay;

            webView.ScrollbarFadingEnabled = false;
        }
    }
}

