﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Webkit;

namespace MyWebView.Droid
{
	[Activity (Label = "MyWebView.Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
	

		protected override void OnCreate (Bundle bundle)
		{
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
            WebView webView = FindViewById<WebView>(Resource.Id.myWebView);

            //Load a particular website
            webView.SetWebViewClient(new WebViewClient());
            webView.LoadUrl("http://www.apple.com");

            //Enable Java
            webView.Settings.JavaScriptEnabled = true;

            //Allow to pan out
            webView.Settings.BuiltInZoomControls = true;
            webView.Settings.SetSupportZoom(true);

            // all scrolling
            webView.ScrollBarStyle = Android.Views.ScrollbarStyles.InsideOverlay;
            webView.ScrollbarFadingEnabled = false;


			
			
		}
	}
}


